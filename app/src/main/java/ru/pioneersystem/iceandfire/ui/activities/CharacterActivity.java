package ru.pioneersystem.iceandfire.ui.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ru.pioneersystem.iceandfire.R;
import ru.pioneersystem.iceandfire.data.managers.DataManager;
import ru.pioneersystem.iceandfire.data.storage.models.Aliase;
import ru.pioneersystem.iceandfire.data.storage.models.Character;
import ru.pioneersystem.iceandfire.data.storage.models.Title;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

public class CharacterActivity extends BaseActivity {
    private static final String TAG = "CharacterActivity";

    private DataManager mDataManager;
    private TextView mHouseWords;
    private TextView mCharacterBorn;
    private TextView mCharacterTitles;
    private TextView mCharacterAliases;
    private Button mCharacterFatherBtn;
    private Button mCharacterMotherBtn;
    private ImageView mToolbarImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);

        mDataManager = DataManager.getInstance();

        String characterUrl = getIntent().getStringExtra("character_url");
        int clanLogoR = getIntent().getIntExtra("clan_logo", 0);
        String houseWords = getIntent().getStringExtra("house_words");
        Character character = mDataManager.getCharacterByUrl(characterUrl);
        String aliaseStr = "";
        List<Aliase> aliases = mDataManager.getAliasesByUrl(characterUrl);
        for (Aliase aliase : aliases) {
            aliaseStr = aliaseStr + aliase.getAliase() + "\r\n";
        }

        String titleStr = "";
        List<Title> titles = mDataManager.getTitlesByUrl(characterUrl);
        for (Title title : titles) {
            titleStr = titleStr + title.getTitle() + "\r\n";
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(character.getName());
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mDataManager = DataManager.getInstance();

        mHouseWords = (TextView) findViewById(R.id.house_words);
        mCharacterBorn = (TextView) findViewById(R.id.character_born);
        mCharacterTitles = (TextView) findViewById(R.id.character_titles);
        mCharacterAliases = (TextView) findViewById(R.id.character_aliases);
        mCharacterFatherBtn = (Button) findViewById(R.id.character_father_btn);
        mCharacterMotherBtn = (Button) findViewById(R.id.character_mother_btn);
        mToolbarImg = (ImageView) findViewById(R.id.toolbar_img);

        mCharacterBorn.setText(character.getBorn());
        mCharacterTitles.setText(titleStr);
        mCharacterAliases.setText(aliaseStr);
        mHouseWords.setText(houseWords);

        Picasso.with(this)
                .load(clanLogoR)
                .fit()
                .placeholder(R.color.colorPrimary)
                .into(mToolbarImg);

        if (!character.getDied().equals("")) {
            showSnackbar(character.getDied());
        }


    }


}
