package ru.pioneersystem.iceandfire.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ru.pioneersystem.iceandfire.R;
import ru.pioneersystem.iceandfire.data.storage.models.Character;
import ru.pioneersystem.iceandfire.data.storage.models.House;
import ru.pioneersystem.iceandfire.ui.activities.CharacterActivity;

public class CharactersAdapter extends RecyclerView.Adapter<CharactersAdapter.CharacterViewHolder> {
    private Context mContext;
    private List<Character> mCharacters;
    private House mHouse;
    private int mCoatOfArmsImg;
    private int mClanLogo;

    public CharactersAdapter(List<Character> characters, House house, int coatOfArmsImg, int clanLogo) {
        mCharacters = characters;
        mHouse = house;
        mCoatOfArmsImg = coatOfArmsImg;
        mClanLogo = clanLogo;
    }

    @Override
    public CharacterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View convertView = LayoutInflater.from(mContext).inflate(R.layout.item_character_list, parent, false);
        return new CharacterViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(CharacterViewHolder holder, int position) {
        final Character character = mCharacters.get(position);

        holder.mCharacterName.setText(character.getName());
//        holder.mCoatOfArmsIco.setImageResource(mCoatOfArmsImg);
        holder.mItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CharacterActivity.class);
                intent.putExtra("character_url", character.getUrl());
                intent.putExtra("clan_logo", mClanLogo);
                intent.putExtra("house_words", mHouse.getWords());
                mContext.startActivity(intent);
            }
        });

        Picasso.with(mContext)
                .load(mCoatOfArmsImg)
                .fit()
                .centerCrop()
                .into(holder.mCoatOfArmsIco);
    }

    @Override
    public int getItemCount() {
        return mCharacters.size();
    }

    public static class CharacterViewHolder extends RecyclerView.ViewHolder {
        private TextView mCharacterName;
        private TextView mCoatOfArms;
        private ImageView mCoatOfArmsIco;
        private LinearLayout mItemLayout;


        public CharacterViewHolder(View itemView) {
            super(itemView);

            mCharacterName = (TextView) itemView.findViewById(R.id.character_name);
            mCoatOfArms = (TextView) itemView.findViewById(R.id.coat_of_arms);
            mCoatOfArmsIco = (ImageView) itemView.findViewById(R.id.coat_of_arms_ico);
            mItemLayout = (LinearLayout) itemView.findViewById(R.id.item_layout);
        }
    }
}
