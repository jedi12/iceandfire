package ru.pioneersystem.iceandfire.ui.activities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

public class BaseActivity extends AppCompatActivity {
    private static final String TAG = "BaseActivity";
    protected ProgressDialog mProgressDialog;

//    public void showProgress() {
//        mProgressDialog = new ProgressDialog(this, R.style.custom_dialog);
//        mProgressDialog.setCancelable(false);
//        mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        mProgressDialog.show();
//        mProgressDialog.setContentView(R.layout.progress_circle);
//    }
//
//    public void hideProgress() {
//        if (mProgressDialog != null) {
//            if (mProgressDialog.isShowing()) {
//                mProgressDialog.hide();
//                mProgressDialog.dismiss();
//            }
//        }
//    }
//
//    public void showSplash() {
//        mProgressDialog = new ProgressDialog(this, R.style.custom_dialog);
//        mProgressDialog.setCancelable(false);
//        mProgressDialog.show();
//        mProgressDialog.setContentView(R.layout.splash_screen);
//    }
//
//    public void hideSplash() {
//        if (mProgressDialog != null) {
//            if (mProgressDialog.isShowing()) {
//                mProgressDialog.hide();
//                mProgressDialog.dismiss();
//            }
//        }
//    }

    public void showSnackbar(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
