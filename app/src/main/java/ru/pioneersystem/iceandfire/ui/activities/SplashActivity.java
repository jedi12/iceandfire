package ru.pioneersystem.iceandfire.ui.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.pioneersystem.iceandfire.R;
import ru.pioneersystem.iceandfire.data.managers.DataManager;
import ru.pioneersystem.iceandfire.data.network.responses.CharactersListRes;
import ru.pioneersystem.iceandfire.data.network.responses.HouseListRes;
import ru.pioneersystem.iceandfire.data.storage.models.Aliase;
import ru.pioneersystem.iceandfire.data.storage.models.AliaseDao;
import ru.pioneersystem.iceandfire.data.storage.models.Character;
import ru.pioneersystem.iceandfire.data.storage.models.CharacterDao;
import ru.pioneersystem.iceandfire.data.storage.models.House;
import ru.pioneersystem.iceandfire.data.storage.models.HouseDao;
import ru.pioneersystem.iceandfire.data.storage.models.SwornMember;
import ru.pioneersystem.iceandfire.data.storage.models.SwornMemberDao;
import ru.pioneersystem.iceandfire.data.storage.models.Title;
import ru.pioneersystem.iceandfire.data.storage.models.TitleDao;
import ru.pioneersystem.iceandfire.utils.AppConfig;
import ru.pioneersystem.iceandfire.utils.MessageEvent;
import ru.pioneersystem.iceandfire.utils.NetworkStatusChecker;

public class SplashActivity extends BaseActivity {
    private static final String TAG = "SplashActivity";
    private static final String NETWORK_NOT_AVAILABLE = "NETWORK_NOT_AVAILABLE";
    private static final String NETWORK_ERROR = "NETWORK_ERROR";
    private static final String HOUSES_OR_CHARACTER_LIST_UPDATED = "HOUSES_OR_CHARACTER_LIST_UPDATED";
    private static int LOADED_HOUSES_PAGES = 0;
    private static int LOADED_CHARACTERS_PAGES = 0;
    private static int HOUSES_PAGES = 9;
    private static int CHARACTERS_PAGES = 43;
    private static boolean SPLASH_SCREEN_DELAY_COMPLETE = false;

    private DataManager mDataManager;
    private HouseDao mHouseDao;
    private SwornMemberDao mSwornMemberDao;
    private CharacterDao mCharacterDao;
    private AliaseDao mAliaseDao;
    private TitleDao mTitleDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mDataManager = DataManager.getInstance();
        mHouseDao = mDataManager.getDaoSession().getHouseDao();
        mSwornMemberDao= mDataManager.getDaoSession().getSwornMemberDao();
        mCharacterDao= mDataManager.getDaoSession().getCharacterDao();
        mAliaseDao= mDataManager.getDaoSession().getAliaseDao();
        mTitleDao= mDataManager.getDaoSession().getTitleDao();

        new DownloadHousesTask().execute();
        new DownloadCharactersTask().execute();
        new WaitTask().execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {

        switch (event.message) {
            case NETWORK_NOT_AVAILABLE:
                showSnackbar("Сеть недоступна");

                break;

            case NETWORK_ERROR:
                showSnackbar("Сервер недоступен");
                if (SPLASH_SCREEN_DELAY_COMPLETE && LOADED_HOUSES_PAGES >= HOUSES_PAGES && LOADED_CHARACTERS_PAGES >= CHARACTERS_PAGES) {
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }

                break;

            case HOUSES_OR_CHARACTER_LIST_UPDATED:
                if (SPLASH_SCREEN_DELAY_COMPLETE && LOADED_HOUSES_PAGES >= HOUSES_PAGES && LOADED_CHARACTERS_PAGES >= CHARACTERS_PAGES) {
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }

                break;
        }
    }

    class WaitTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(AppConfig.START_DELAY);
                SPLASH_SCREEN_DELAY_COMPLETE = true;
                EventBus.getDefault().post(new MessageEvent(HOUSES_OR_CHARACTER_LIST_UPDATED));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    class DownloadHousesTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            if (!NetworkStatusChecker.isNetworkAvailable(SplashActivity.this)) {
                EventBus.getDefault().post(new MessageEvent(NETWORK_NOT_AVAILABLE));
                return null;
            }

            for (int page = 1; page <= HOUSES_PAGES; page++) {
                Call<List<HouseListRes>> call = mDataManager.getHouses(page);
                call.enqueue(new Callback<List<HouseListRes>>() {
                    @Override
                    public void onResponse(Call<List<HouseListRes>> call, Response<List<HouseListRes>> response) {
                        try {
                            if (response.code() == 200) {

                                List<House> houses = new ArrayList<>();
                                List<SwornMember> swornMembers = new ArrayList<>();

                                for (HouseListRes houseListRes: response.body()) {
                                    houses.add(new House(houseListRes));
                                    swornMembers.addAll(getSwornMembers(houseListRes));
                                }

                                mSwornMemberDao.insertOrReplaceInTx(swornMembers);
                                mHouseDao.insertOrReplaceInTx(houses);

                                LOADED_HOUSES_PAGES++;
                                EventBus.getDefault().post(new MessageEvent(HOUSES_OR_CHARACTER_LIST_UPDATED));
                            } else {
                                LOADED_HOUSES_PAGES++;
                                EventBus.getDefault().post(new MessageEvent(NETWORK_ERROR));
                                Log.e(TAG, "Network response: " + response.code() + " - " + response.errorBody());
                            }
                        } catch (NullPointerException e){
                            LOADED_HOUSES_PAGES++;
                            EventBus.getDefault().post(new MessageEvent(NETWORK_ERROR));
                            Log.e(TAG, "Network exception: " + e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<HouseListRes>> call, Throwable t) {
                        LOADED_HOUSES_PAGES++;
                        EventBus.getDefault().post(new MessageEvent(NETWORK_ERROR));
                        Log.e(TAG, "Network failure: " + t.getMessage());
                    }
                });
            }
            return null;
        }
    }

    class DownloadCharactersTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            if (!NetworkStatusChecker.isNetworkAvailable(SplashActivity.this)) {
                EventBus.getDefault().post(new MessageEvent(NETWORK_NOT_AVAILABLE));
                return null;
            }

            for (int page = 1; page <= CHARACTERS_PAGES; page++) {
                Call<List<CharactersListRes>> call = mDataManager.getCharacters(page);
                call.enqueue(new Callback<List<CharactersListRes>>() {
                    @Override
                    public void onResponse(Call<List<CharactersListRes>> call, Response<List<CharactersListRes>> response) {
                        try {
                            if (response.code() == 200) {

                                List<Character> characters = new ArrayList<>();
                                List<Aliase> aliases = new ArrayList<>();
                                List<Title> titles = new ArrayList<>();

                                for (CharactersListRes charactersListRes: response.body()) {
                                    characters.add(new Character(charactersListRes));
                                    aliases.addAll(getAliases(charactersListRes));
                                    titles.addAll(getTitles(charactersListRes));
                                }

                                mAliaseDao.insertOrReplaceInTx(aliases);
                                mTitleDao.insertOrReplaceInTx(titles);
                                mCharacterDao.insertOrReplaceInTx(characters);

                                LOADED_CHARACTERS_PAGES++;
                                EventBus.getDefault().post(new MessageEvent(HOUSES_OR_CHARACTER_LIST_UPDATED));
                            } else {
                                LOADED_CHARACTERS_PAGES++;
                                EventBus.getDefault().post(new MessageEvent(NETWORK_ERROR));
                                Log.e(TAG, "Network response: " + response.code() + " - " + response.errorBody());
                            }
                        } catch (NullPointerException e){
                            LOADED_CHARACTERS_PAGES++;
                            EventBus.getDefault().post(new MessageEvent(NETWORK_ERROR));
                            Log.e(TAG, "Network exception: " + e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<List<CharactersListRes>> call, Throwable t) {
                        LOADED_CHARACTERS_PAGES++;
                        EventBus.getDefault().post(new MessageEvent(NETWORK_ERROR));
                        Log.e(TAG, "Network failure: " + t.getMessage());
                    }
                });
            }
            return null;
        }
    }

    private List<SwornMember> getSwornMembers(final HouseListRes houseListRes) {
        List<SwornMember> swornMembers = new ArrayList<>();
        for (String character : houseListRes.swornMembers) {
            swornMembers.add(new SwornMember(houseListRes.url, character));
        }
        return swornMembers;
    }

    private List<Aliase> getAliases(final CharactersListRes charactersListRes) {
        List<Aliase> aliases = new ArrayList<>();
        for (String character : charactersListRes.aliases) {
            aliases.add(new Aliase(charactersListRes.url, character));
        }
        return aliases;
    }

    private List<Title> getTitles(final CharactersListRes charactersListRes) {
        List<Title> titles = new ArrayList<>();
        for (String character : charactersListRes.titles) {
            titles.add(new Title(charactersListRes.url, character));
        }
        return titles;
    }
}
