package ru.pioneersystem.iceandfire.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewGroup;

import java.util.List;

import ru.pioneersystem.iceandfire.R;
import ru.pioneersystem.iceandfire.data.managers.DataManager;
import ru.pioneersystem.iceandfire.data.storage.models.Character;
import ru.pioneersystem.iceandfire.data.storage.models.House;
import ru.pioneersystem.iceandfire.ui.adapters.CharactersAdapter;

import static ru.pioneersystem.iceandfire.R.id.container;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "MainActivity";

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            mViewPager.setCurrentItem(0);
        } else if (id == R.id.nav_gallery) {
            mViewPager.setCurrentItem(1);
        } else if (id == R.id.nav_slideshow) {
            mViewPager.setCurrentItem(2);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";
        public static String STARKS_CLAN_URL = "http://anapioficeandfire.com/api/houses/362";
        public static String LANNISTERS_CLAN_URL = "http://anapioficeandfire.com/api/houses/229";
        public static String TARGARIENS_CLAN_URL = "http://anapioficeandfire.com/api/houses/378";

        private DataManager mDataManager;
        House house;
        List<Character> characters;
        int coatOfArmsImg;
        int clanLogo;

        public PlaceholderFragment() {

        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);

            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            String currUrl = "";
            switch ((int)getArguments().get(ARG_SECTION_NUMBER)) {
                case 1:
                    currUrl = STARKS_CLAN_URL;
                    coatOfArmsImg = R.drawable.stark_icon;
                    clanLogo = R.drawable.stark;
                    break;

                case 2:
                    currUrl = LANNISTERS_CLAN_URL;
                    coatOfArmsImg = R.drawable.lanister_icon;
                    clanLogo = R.drawable.lannister;
                    break;

                case 3:
                    currUrl = TARGARIENS_CLAN_URL;
                    coatOfArmsImg = R.drawable.targarien_ico;
                    clanLogo = R.drawable.targarien;
                    break;
            }

            mDataManager = DataManager.getInstance();
            house = mDataManager.getHouseByUrl(currUrl);
            characters = mDataManager.getCharactersOfHouse(house);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            RecyclerView mRecyclerView = (RecyclerView) rootView.findViewById(R.id.character_list);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            mRecyclerView.setLayoutManager(linearLayoutManager);
            mRecyclerView.swapAdapter(new CharactersAdapter(characters, house, coatOfArmsImg, clanLogo), false);

            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "STARKS";
                case 1:
                    return "LANNISTERS";
                case 2:
                    return "TARGARYENS";
            }
            return null;
        }
    }
}
