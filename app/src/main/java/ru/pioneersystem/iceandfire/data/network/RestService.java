package ru.pioneersystem.iceandfire.data.network;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.pioneersystem.iceandfire.data.network.responses.CharactersListRes;
import ru.pioneersystem.iceandfire.data.network.responses.HouseListRes;

public interface RestService {
    public static final String PAGE_SIZE = "50";

    @GET("houses")
    Call<List<HouseListRes>> getHouses(@Query("pageSize") String pageSize, @Query("page") int page);

    @GET("characters")
    Call<List<CharactersListRes>> getCharacters(@Query("pageSize") String pageSize, @Query("page") int page);

}
