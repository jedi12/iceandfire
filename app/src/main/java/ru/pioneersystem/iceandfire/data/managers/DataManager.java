package ru.pioneersystem.iceandfire.data.managers;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import ru.pioneersystem.iceandfire.data.network.RestService;
import ru.pioneersystem.iceandfire.data.network.ServiceGenerator;
import ru.pioneersystem.iceandfire.data.network.responses.CharactersListRes;
import ru.pioneersystem.iceandfire.data.network.responses.HouseListRes;
import ru.pioneersystem.iceandfire.data.storage.models.Aliase;
import ru.pioneersystem.iceandfire.data.storage.models.AliaseDao;
import ru.pioneersystem.iceandfire.data.storage.models.Character;
import ru.pioneersystem.iceandfire.data.storage.models.CharacterDao;
import ru.pioneersystem.iceandfire.data.storage.models.DaoSession;
import ru.pioneersystem.iceandfire.data.storage.models.House;
import ru.pioneersystem.iceandfire.data.storage.models.HouseDao;
import ru.pioneersystem.iceandfire.data.storage.models.SwornMember;
import ru.pioneersystem.iceandfire.data.storage.models.SwornMemberDao;
import ru.pioneersystem.iceandfire.data.storage.models.Title;
import ru.pioneersystem.iceandfire.data.storage.models.TitleDao;
import ru.pioneersystem.iceandfire.utils.App;

public class DataManager {
    private static DataManager INSTANCE = null;

    private Context mContext;
    private RestService mRestService;
    private DaoSession mDaoSession;

    public DataManager() {
        mContext = App.getContext();
        mRestService = ServiceGenerator.createService(RestService.class);
        mDaoSession = App.getDaoSession();
    }

    public static DataManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DataManager();
        }

        return INSTANCE;
    }

    public Context getContext() {
        return mContext;
    }


    // ====================================  Network  ====================================

    public Call<List<HouseListRes>> getHouses(int page) {
        return mRestService.getHouses(RestService.PAGE_SIZE, page);
    }

    public Call<List<CharactersListRes>> getCharacters(int page) {
        return mRestService.getCharacters(RestService.PAGE_SIZE, page);
    }



    // ====================================  Database  ===================================

    public DaoSession getDaoSession() {
        return mDaoSession;
    }

    public House getHouseByUrl(String url) {
        House house = null;
        try {
            house = mDaoSession.queryBuilder(House.class)
                    .where(HouseDao.Properties.Url.eq(url))
                    .build()
                    .unique();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return house;
    }

    public Character getCharacterByUrl(String characterUrl) {
        Character character = null;
        try {
            character = mDaoSession.queryBuilder(Character.class)
                    .where(CharacterDao.Properties.Url.eq(characterUrl))
                    .build()
                    .unique();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return character;
    }

    public List<Aliase> getAliasesByUrl(String characterUrl) {
        List<Aliase> aliases = new ArrayList<>();
        try {
            aliases = mDaoSession.queryBuilder(Aliase.class)
                    .where(AliaseDao.Properties.AliaseUrl.eq(characterUrl))
                    .build()
                    .list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return aliases;
    }

    public List<Title> getTitlesByUrl(String characterUrl) {
        List<Title> titles = new ArrayList<>();
        try {
            titles = mDaoSession.queryBuilder(Title.class)
                    .where(TitleDao.Properties.TitleUrl.eq(characterUrl))
                    .build()
                    .list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return titles;
    }

    public List<Character> getCharactersOfHouse(House house) {
        List<Character> characters = new ArrayList<>();

        List<String> chadactersUrl = new ArrayList<>();
        for (SwornMember swornMember : house.getSwornMembers()) {
            chadactersUrl.add(swornMember.getCharacter());
        }

        try {
            characters = mDaoSession.queryBuilder(Character.class)
                    .where(CharacterDao.Properties.Url.in(chadactersUrl))
                    .orderAsc(CharacterDao.Properties.Name)
                    .build()
                    .list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return characters;
    }
}
