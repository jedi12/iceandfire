package ru.pioneersystem.iceandfire.utils;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;

import org.greenrobot.greendao.database.Database;

import ru.pioneersystem.iceandfire.data.storage.models.DaoMaster;
import ru.pioneersystem.iceandfire.data.storage.models.DaoSession;

public class App extends Application {

    private static Context sContext;
    private static DaoSession sDaoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "ice_and_fire-db");
        Database db = helper.getWritableDb();
        sDaoSession = new DaoMaster(db).newSession();

        Stetho.initializeWithDefaults(this);
    }

    public static Context getContext() {
        return sContext;
    }

    public static DaoSession getDaoSession() {
        return sDaoSession;
    }
}
