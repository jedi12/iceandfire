package ru.pioneersystem.iceandfire.utils;

public interface AppConfig {
    String BASE_URL = "http://anapioficeandfire.com/api/";
    long MAX_CONNECT_TIMEOUT = 5000;
    long MAX_READ_TIMEOUT = 5000;
    int START_DELAY = 3000;
}
