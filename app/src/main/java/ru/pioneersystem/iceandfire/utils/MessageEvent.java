package ru.pioneersystem.iceandfire.utils;

public class MessageEvent {
    public final String message;

    public MessageEvent(String message) {
        this.message = message;
    }
}